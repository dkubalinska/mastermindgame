﻿nrGuesses = 8;
codeLength = 4;
nrColors = 6;

document.addEventListener("DOMContentLoaded", dokumentGeladen, false);

function dokumentGeladen(e) {
    document.body.insertAdjacentHTML('beforeend', generateGameBoard(nrGuesses, codeLength, nrColors));
    document.body.insertAdjacentHTML('afterbegin', generateInstruction());
    setGameParameters();
    enableRow(level,codeLength);
    hiddenColor = generateHiddenColors(codeLength, nrColors);
    document.getElementById('newGame').addEventListener('click', newGame);
    document.getElementById('checkResult').addEventListener('click', checkResult);
}

//button check result (Eingabe prüfen) clicked
function checkResult() {
    //all pins must be filled
    if (columnColorsIndex.includes(-1)) {
        alert('Please color all the elements in the level ' + level);
        return;
    }
    var nrElementsWithCorrectColorOrPosition = checkNrElementsWithCorrectColorOrPosition();
    scorePegs(nrElementsWithCorrectColorOrPosition);
    disableRow(level, codeLength);

    if (nrElementsWithCorrectColorOrPosition[0] === codeLength || level === 10) {
        showHiddenPins();
        alert('Congratulations!!! You needed only ' + level + ' trials to find the solution.');
    }
    else if (level === nrGuesses) {
        showHiddenPins();
        alert('You reached the maximal number of trials. Play again!!!!!');
    }
    else {
        level += 1;
        enableRow(level, codeLength);
    }
}

//---button New Game (Neues Spiel) clicked -----
function newGame() {
    hiddenColor = generateHiddenColors(codeLength, nrColors);
    resetGameColors();
    disableRow(level, codeLength);
    level = 1;
    enableRow(level, codeLength);
}

function setGameParameters() {
    level = 1;
    colors = new Array();
    for (var i = 1; i <= nrColors; i++) {
        colors.push(getComputedStyle(document.getElementById('pinCode' + i)).backgroundColor);
    }
    columnColorsIndex = new Array(codeLength).fill(-1);
}

//Change the color of the clicked pin
function changeColor(e) {
    var myDiv = document.getElementById(e.target.id);
    var column = (Number)(myDiv.id.substr(-1));
    columnColorsIndex[column - 1] += 1;
    columnColorsIndex[column - 1] = columnColorsIndex[column - 1] % nrColors;
    var index = columnColorsIndex[column - 1];
    myDiv.style.backgroundColor = colors[index];
}

function checkNrElementsWithCorrectColorOrPosition() {
    var chosenColors = new Array();
    for (var i = 0; i < codeLength; i++) {
        chosenColors.push(colors[columnColorsIndex[i]]);
    }
    var nrCorrectPosition = 0; //nr of Elements with correct position and correct color
    var nrCorrectColor = 0;    //nr of Elements yith wrong position but correct color
    var isCorrectPositionAndColor = new Array(codeLength).fill(0);

   //1.search for elements with correct color on correct position
    for (var i = 0; i < codeLength; i++) {
        if (hiddenColor[i] === chosenColors[i]) {
            nrCorrectPosition += 1;
            chosenColors[i] = '';
            isCorrectPositionAndColor[i] = 1;
        }
    }
    //2. search for elements with correct color but wrong position
    for (var i = 0; i < codeLength; i++) {
        if (isCorrectPositionAndColor[i] !== 1) {
            var temp = chosenColors.indexOf(hiddenColor[i]);
            if (temp > -1) {
                nrCorrectColor += 1;
                chosenColors[temp] = '';
            }
        }
    }
    columnColorsIndex = new Array(codeLength).fill(-1);
    return [nrCorrectPosition, nrCorrectColor];    
}

function scorePegs(nrElementsWithCorrectColorOrPosition) {
    var result = 'result' + level;
    if (!nrElementsWithCorrectColorOrPosition[0] == 0) {
        for (var i = 1; i <= nrElementsWithCorrectColorOrPosition[0]; i++) {
            document.getElementById(result + String(i)).classList.add('correctPosition');
        }
    }
    if (!nrElementsWithCorrectColorOrPosition[1] == 0) {
        for (var i = 1 + nrElementsWithCorrectColorOrPosition[0]; i <= nrElementsWithCorrectColorOrPosition[1] + nrElementsWithCorrectColorOrPosition[0]; i++) {
            document.getElementById(result + String(i)).classList.add('correctColor');
        }
    }
    for (var i = nrElementsWithCorrectColorOrPosition[1] + nrElementsWithCorrectColorOrPosition[0] + 1; i <= codeLength; i++) {
        document.getElementById(result + String(i)).style.backgroundColor = 'initial';
    }

}

function showHiddenPins() {
    for (var i = 1; i < codeLength+1; i++) {
        document.getElementById('pinGen' + String(i)).style.backgroundColor = hiddenColor[i - 1];
    }
}


//-----Row/Level enable
function enableRow(rowNr, codeLength) {
    pin = '#pin' + rowNr;
    pinString = '';
    for (var i = 1; i < codeLength; i++) {
        pinString+= pin + i+', ';
    }
    pinString += pin + codeLength;
    var pins = document.querySelectorAll(pinString);
    for (var i = 0; i < pins.length; i++) {
        pins[i].addEventListener('click', changeColor);
    }
}

//-----Row/Level disable
function disableRow(rowNr, codeLength) {
    pin = '#pin' + rowNr;
    pinString = '';
    for (var i = 1; i < codeLength; i++) {
        pinString += pin + i + ', ';
    }
    pinString += pin + codeLength;
    var pins = document.querySelectorAll(pinString);
    for (var i = 0; i < pins.length; i++) {
        pins[i].removeEventListener('click', changeColor);
    }
}


//--- Generate the hidden colors-------
function generateHiddenColors(codeLength,nrColors) {
    var hiddenColors = [];
    for (var i = 0; i < codeLength; i++) {
        hiddenColors[i] = colors[getRandomInt(0, nrColors-1)];
    }
    return hiddenColors;
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//-------Reset colors---------------------------
function resetGameColors() {
    var pins = document.querySelectorAll("div[id*=pin]");
    var resultPins = document.querySelectorAll("div[id*=result]");
    resetColors(pins);
    resetColors(resultPins);
}

function resetColors(elements) {
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor = '';
        elements[i].classList.remove('correctColor');
        elements[i].classList.remove('correctPosition');
    }
}

//----Generate the HTML Code for the Board----BEGIN----------
function generateGameBoard(nrGuesses, codeLength, nrColors) {
    var gameBoardHTML = '<div id="Container"><h2>Mastermind</h2>';
    gameBoardHTML += generateHiddenPins(codeLength);
    gameBoardHTML += generateTargetZone(nrGuesses, codeLength);
    gameBoardHTML += generatePinCodeRow(nrColors);
    gameBoardHTML += generateFooter();
    gameBoardHTML += '</div>';
    return gameBoardHTML;
}
//generate the target zone
function generateTargetZone(nrGuesses, codeLength) {
    var targetZoneHTML = '<div id="targetZone">';
    for (var rowNr = nrGuesses; rowNr >= 1; rowNr--) {
        targetZoneHTML += generateRow(rowNr, codeLength);
    }
    return targetZoneHTML;
}

//generate a row of the game
function generateRow(rowNr, codeLength) {
    var rowHTML = '<div id="row' + rowNr + '">';
    rowHTML += generateRowPart(rowNr, codeLength, "pin");
    rowHTML += '<div class="resultGrid">';
    rowHTML += generateRowPart(rowNr, codeLength, "result");
    rowHTML += '</div></div>';
    return rowHTML;
}

function generateRowPart(rowNr, codeLength, pinName) {
    var rowHTML = ''; 
    for (var i = 1; i <= codeLength; i++) {
        rowHTML += '<div id=' + pinName + rowNr + i + '></div>';
    }
    return rowHTML;
}

//generate possible pin codes (choice of colors)
function generatePinCodeRow(nrColors) {
    var pinCodesHTML = '<hr /><div class="pinCodes">';
    for (var i = 1; i <= nrColors; i++) {
        pinCodesHTML += '<div id="pinCode' + i + '" data-code="' + i + '"></div>';
    }
    return pinCodesHTML+'</div>';
}

function generateFooter() {
    return '<footer>'
        + '<input type = "button" id = "checkResult" class= "button" value = "Check" />'
        + '<input type="button" id="newGame" class="button" value="New game" />'
        + ' </footer>';
}

function generateHiddenPins(codeLength) {
    var hiddenPinsHTML = '<hr /><div id="genrow">';
    hiddenPinsHTML += generateRowPart('', codeLength, "pinGen");
    hiddenPinsHTML += '</div><hr />';
    return hiddenPinsHTML;
}

function generateInstruction() {
    var gameInstruction = '<div id="Description"><h3>Instruction</h3><h5>' + generateGameDescriptionText() + '</h5>';
    gameInstruction += generateGameInstructionText();
    gameInstruction += '<div id = "PinDescription" ><div class="resultGrid"><div id="CorrectPosition" class="correctPosition"></div><h5>correct color and correct position</h5>';
    gameInstruction += '<div id="CorrectColor" class="correctColor"></div><h5>correct color but wrong position</h5></div> </div></div>';
    return gameInstruction;
}

function generateGameDescriptionText() {
    return "The idea of the game is for the player (the code-breaker) to guess "
        + "the secret code chosen by the computer(the code-maker). The code is a sequence of 4 colored pegs "
        + "chosen from six colors available. The code-breaker makes a serie of pattern guesses - after each guess "
        + "the code-maker gives feedback in the form of 2 numbers, the number of pegs that are of the right color "
        + "and in the correct position, and the number of pegs that are of the correct color but not in the correct position.";
}

function generateGameInstructionText() {
    return "<h5> 1. Place your first guess in the lowest row of the board. (Use your left mouse click to change the color of the peg).</h5>"
        + "<h5> 2. Click the button '" + document.getElementById('checkResult').getAttribute('value') + "'.</h5>"
        + "<h5> 3. Repeat with the next row.</h5>"
        + "<h5> 4. Continue until the code is guessed or there are no more guesses left.</h5>";
}